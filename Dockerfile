# Build container for ruok
FROM golang:1.18 as build-ruok
WORKDIR /go/src/ruok
COPY . .
RUN go mod download
RUN go build -o /go/bin/ruok cmd/main.go

# Build container for example go app
FROM golang:1.18 as build-hello
WORKDIR /go/src/hello
COPY ./example .
RUN go mod download
RUN go build -o /go/bin/hello main.go

# Image that extends the distroless image by adding ruok to binaries
FROM gcr.io/distroless/cc-debian11 as distroless-golang-ruok
COPY --from=build-ruok /go/bin/ruok /bin/ruok

# Test container
FROM gcr.io/distroless/cc-debian11 as test
COPY --from=build-ruok /go/bin/ruok /bin/ruok
COPY --from=build-hello /go/bin/hello /bin/hello
HEALTHCHECK --interval=10s --retries=3 --start-period=5s --timeout=10s CMD ["ruok", "--verbose", "--port", "8080", "--path", "/health"]
CMD ["hello"]
