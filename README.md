# RUOK

A simple all-in-one golang CLI which sends a GET HTTP request to a local healthcheck endpoint. The CLI exits with 0 if it receives a HTTP response with a 2XX status code. The CLI exits with 1 otherwise.

## Motivation

This tool is meant to be used in Docker container healtchcheck. It pings a local healtchcheck endpoint to perform healthcheck. Using this method has the following benefits :
- Allows healthcheck to be performed by the container runtime without network operations
- No need to expose container ports as the CLI uses localhost interface
- No need to use curl or wget that can be leveraged in attacks
- The tool cannnot be used to explore your container network as it enforces localhost as target for HTTP requests
- Suitable for distroless containers

## Build

```bash
go build -o ruok ./cmd/main.go
```

## Usage

CLI can be configured either from CLI args or from ENV

```text
NAME:
   ruok - ping your local healthcheck http endpoint and return 0 in case of scuess or -1 in case of failure.

USAGE:
   ruok [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --port value    Port to use for healthcheck HTTP request (default: 8080) [$RUOK_PORT]
   --path value    Empty string or URL path with a leading slash to use for healthcheck HTTP request (default: /health) [$RUOK_PATH]
   --scheme value  Scheme (http or https) to use for healthcheck HTTP request (default: http) [$RUOK_SCHEME]
   --verbose       Activate debug logs on stdout (default: false) [$RUOK_VERBOSE]
   --help, -h      show help (default: false)
```

## Test with container healthcheck

You can familiarize with container healthchek with the following lab : https://dockerlabs.collabnix.com/beginners/dockerfile/healthcheck.html

As exeample, A modified Dockerfile is provided. It includes :
- A build container which builds the CLI 
- A build container which builds the example golang application taht will be probed using RUOK.
- A distroless container image which embeds both the CLI and the golang application to run.

Build the container : 

```bash
docker build --target test -t ruok .
```

Run the container :

```bash
docker run ruok
```

Check the container status :

```bash
docker ps -a
```

You should see the following line :

```text
CONTAINER ID   IMAGE                COMMAND          CREATED          STATUS                      PORTS     NAMES
d5ca2bb36a19   ruok                 "/hello"         14 seconds ago   Up 13 seconds (healthy)               flamboyant_goldstine
```

## Using RUOK for your containers

Instead of building from source, you can either
- Use a build stage to download ruok binairy from a package registry
- Use distroless-golang-ruok as base image