package flags

import (
	"fmt"
	"regexp"
	"strconv"

	"github.com/urfave/cli/v2"
	"gitlab.com/lake42/ruok/internal/constants"
)

// Return configured CLI flag for scheme
func GetSchemeFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        constants.RUOK_SCHEME_FLAG,
		Required:    false,
		Value:       constants.RUOK_SCHEME_DEFAULT,
		Usage:       "Scheme (http or https) to use for healthcheck HTTP request",
		DefaultText: constants.RUOK_SCHEME_DEFAULT,
		EnvVars:     []string{constants.RUOK_SCHEME_ENVVAR},
		Action: func(ctx *cli.Context, v string) error {
			// Validate flag value
			if v != "http" && v != "https" {
				return fmt.Errorf("flag value can be http or https. Got %v", v)
			}
			return nil
		},
	}
}

// Return configured CLI flag for port
func GetPortFlag() *cli.Int64Flag {
	return &cli.Int64Flag{
		Name:        constants.RUOK_PORT_FLAG,
		Required:    false,
		Value:       constants.RUOK_PORT_DEFAULT,
		Usage:       "Port to use for healthcheck HTTP request",
		DefaultText: strconv.FormatInt(constants.RUOK_PORT_DEFAULT, 10),
		EnvVars:     []string{constants.RUOK_PORT_ENVVAR},
		Action: func(ctx *cli.Context, v int64) error {
			if v >= 65536 || v < 0 {
				return fmt.Errorf("flag port value %v out of range[0-65535]", v)
			}
			return nil
		},
	}
}

// Return configured CLI flag for url path
func GetPathFlag() *cli.StringFlag {
	return &cli.StringFlag{
		Name:        constants.RUOK_PATH_FLAG,
		Required:    false,
		Value:       constants.RUOK_PATH_DEFAULT,
		Usage:       "Empty string or URL path with a leading slash to use for healthcheck HTTP request",
		DefaultText: constants.RUOK_PATH_DEFAULT,
		EnvVars:     []string{constants.RUOK_PATH_ENVVAR},
		Action: func(ctx *cli.Context, v string) error {
			// Validate flag value - Must be empty or start with a slash
			if !regexp.MustCompile(`^$|^\/.*$`).MatchString(v) {
				return fmt.Errorf("flag value must be empty or start with a slash. Got %v", v)
			}
			return nil
		},
	}
}

// Return configured CLI flag for verbose mode
func GetVerboseFlag() *cli.BoolFlag {
	return &cli.BoolFlag{
		Name:        constants.RUOK_VERBOSE_FLAG,
		Required:    false,
		Value:       constants.RUOK_VERBOSE_DEFAULT,
		Usage:       "Activate debug logs on stdout",
		DefaultText: strconv.FormatBool(constants.RUOK_VERBOSE_DEFAULT),
		EnvVars:     []string{constants.RUOK_VERBOSE_ENVVAR},
	}
}
