package healthcheck_test

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lake42/ruok/internal/healthcheck"
)

func TestLocalHealthCheckSuccess200(t *testing.T) {

	// Local HTTP server with healthcheck handler - Always successful
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Return successful healthcheck
		w.Write([]byte("OK"))
		w.WriteHeader(200)
	}))
	defer srv.Close()

	// Get local server port
	port, err := strconv.ParseInt(strings.Split(srv.URL, ":")[2], 10, 64)
	assert.NoError(t, err)

	// Test local healthcheck
	expected := 0
	actual := healthcheck.LocalHealthcheck("http", port, "", &log.Logger)

	// Assertions
	assert.Equal(t, expected, actual)
}

func TestLocalHealthCheckSuccess299(t *testing.T) {

	// Local HTTP server with healthcheck handler - Always successful
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Check that request has /health as path
		assert.Equal(t, "/health", r.URL.Path)
		// Return successful healthcheck - other than 200
		w.Write([]byte("OK"))
		w.WriteHeader(299)
	}))
	defer srv.Close()

	// Get local server port
	port, err := strconv.ParseInt(strings.Split(srv.URL, ":")[2], 10, 64)
	assert.NoError(t, err)

	// Test local healthcheck
	expected := 0
	actual := healthcheck.LocalHealthcheck("http", port, "/health", &log.Logger)

	// Assertions
	assert.Equal(t, expected, actual)
}

func TestLocalHealthCheckFail(t *testing.T) {

	// Local HTTP server with healthcheck handler - Always successful
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Return unsuccessful healthcheck - 500
		w.WriteHeader(500)
	}))
	defer srv.Close()

	// Get local server port
	port, err := strconv.ParseInt(strings.Split(srv.URL, ":")[2], 10, 64)
	assert.NoError(t, err)

	// Test local healthcheck
	expected := 1
	actual := healthcheck.LocalHealthcheck("http", port, "", &log.Logger)

	// Assertions
	assert.Equal(t, expected, actual)
}
